<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';



function sitemap_editor_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('sitemap_editor');
    
    $addon->unregisterFunctionality('Ovml/Function/SitemapEditorTopics');
    $addon->unregisterFunctionality('Ovml/Function/SitemapEditorThumbnail');
    $addon->unregisterFunctionality('Ovml/Container/SitemapEditorAttachedFiles');
    $addon->unregisterFunctionality('SitemapEditorNodeSection');
    $addon->unregisterFunctionality('SitemapEditorNode');
    $addon->unregisterFunctionality('SitemapEditorNode/Category');
    $addon->unregisterFunctionality('SitemapEditorNode/Container');
    $addon->unregisterFunctionality('SitemapEditorNode/Ref');
    $addon->unregisterFunctionality('SitemapEditorNode/Topic');
    $addon->unregisterFunctionality('SitemapEditorNode/Url');
    $addon->unregisterFunctionality('ContextActions/SitemapNode');
    
    $addon->removeAllEventListeners();

	return true;
}



function sitemap_editor_upgrade($sVersionBase, $sVersionIni)
{
	global $babBody, $babDB;

	include_once $GLOBALS['babInstallPath'].'admin/acl.php';
	include_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	include_once dirname(__FILE__).'/functions.php';
	include_once dirname(__FILE__).'/node.class.php';

	// attach events

	$addon = bab_getAddonInfosInstance('sitemap_editor');
	$addon->removeAllEventListeners();
	
	
	
	$addon->addEventListener('bab_eventBeforePageCreated',     'smed_applySkin',               'events.php', 10);
	$addon->addEventListener('bab_eventUserDeleted',           'smed_onUserDeleted',           'events.php');
	$addon->addEventListener('bab_eventBeforeSiteMapCreated',  'smed_onBeforeSiteMapCreated',  'events.php');
	$addon->addEventListener('bab_eventBeforeSiteMapList',     'smed_onBeforeSiteMapList',     'events.php');


	$addon->registerFunctionality('Ovml/Function/SitemapEditorTopics', 'ovml_topics.php');
	$addon->registerFunctionality('Ovml/Function/SitemapEditorThumbnail', 'ovml_topics.php');
	$addon->registerFunctionality('Ovml/Container/SitemapEditorAttachedFiles', 'ovml_topics.php');
	$addon->registerFunctionality('SitemapEditorNodeSection', 'nodesection.class.php');
	$addon->registerFunctionality('SitemapEditorNode', 'sitemapeditornode.class.php');
	$addon->registerFunctionality('SitemapEditorNode/Category', 'nodetypes/category/func.class.php');
	$addon->registerFunctionality('SitemapEditorNode/Container', 'nodetypes/container/func.class.php');
	$addon->registerFunctionality('SitemapEditorNode/Ref', 'nodetypes/ref/func.class.php');
	$addon->registerFunctionality('SitemapEditorNode/Topic', 'nodetypes/topic/func.class.php');
	$addon->registerFunctionality('SitemapEditorNode/Url', 'nodetypes/url/func.class.php');
	$addon->registerFunctionality('ContextActions/SitemapNode', 'contextactions.sitemapnode.class.php');


	$content_type = true;
	if (bab_isTable('smed_nodes'))
	{
		// if table exists but without the content_type column
		$content_type = bab_isTableField('smed_nodes', 'content_type');
	}

	$tables = new bab_synchronizeSql();
	$tables->fromSqlFile(dirname(__FILE__).'/dump.sql');


	if ($tables->isEmpty('smed_sitemap')) {
		include_once dirname(__FILE__).'/sitemap.class.php';
		if (null === smed_Sitemap::create('My sitemap')) {
			$babBody->addError('Error, default sitemap can\'t be created');
			return false;
		}

	}

	if (!$content_type)
	{
		bab_installWindow::message(smed_translate('Update nodes types'));

		$res = $babDB->db_query('SELECT * FROM smed_nodes WHERE id_sitemap='.$babDB->quote(1));
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$type = null;
			$node = smed_Node::getFromTable($arr);

			$nodeId = $node->getTarget();
			if (is_object($nodeId)) {
				$nodeId = $nodeId->id_function;

				// target type

				if (substr($nodeId, 0, strlen('babArticle_')) === 'babArticle_') {
					$type = 'article';
				}
				if (substr($nodeId, 0, strlen('babArticleTopic_')) === 'babArticleTopic_') {
					$type = 'topic';
				}
				if (substr($nodeId, 0, strlen('babArticleCategory_')) === 'babArticleCategory_') {
					$type = 'category';
				}
				if (substr($nodeId, 0, strlen('applicationsApp')) === 'applicationsApp') {
					$type = 'url';
				}
			}

			if (substr($nodeId, 0, strlen('smed_')) === 'smed_')
			{
				if ($node->getUrl() != '') {
					$type = 'url';
				} else {
					$type = 'container';
				}
			}

			if (isset($type))
			{
				$node->setContentType($type);
				$node->update();
			}
		}

		// If the appendId config parameter has not been defined, we set it to
		// true by default.
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/sitemap_editor/');
		$appendId = $registry->getValue('appendId');
		if ($appendId === null) {
			$registry->setKeyValue('appendId', true);
		}
	}



	include_once dirname(__FILE__).'/node.class.php';
	include_once dirname(__FILE__).'/default_sitemap.php';
	include_once dirname(__FILE__).'/upgrade.php';

	if (null === smed_Node::getFromId(1, 'Custom'))
	{
		$default = new smed_defaultSitemap();
		$default->create();
	}
	

	smed_moveCustomRights();
	

	return true;
}
