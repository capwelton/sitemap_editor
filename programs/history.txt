08/09/2017 - 0.8.16

- Compatibility with vendor

07/09/2017 - 0.8.15

- Correction d'un bug sur les droits d'accès de la création des sous-noeuds


26/01/2016 - Version 0.7.5

- Modification des ID de noeuds


15/12/2015 - Version 0.7.0

- Ajout de l'API permettant de créer des nouveaux types de contenu en dehors du module
- Ajout des droits d'accès


10/08/2015 - Version 0.6.7

- Amélioration du script de déplacement des thèmes dans administration > articles


04/06/2015 - Version 0.6.6

- Correction du bug pour la redirection vers le premier enfant


20/04/2015 - Version 0.6.5

- Ajout d'une option pour supprimer le thème d'article associé au noeud


05-07-2013 - Version 0.6.0

- modification de la compatiblité avec le noyeau pour l'ajout des noeuds dynamiques (rewriting des articles)


19-06-2013 - Version 0.5.9

- 2 méthodes de classement des sous-noeuds pour les noeuds de type référence


2012-10-23 - Version 0.5.5

- Ajout du prerequis sur les magic_quotes
- correction d'un bug sur la case ignorer dans le menu
- ajout de breadCrumbIgnore (noyau 7.8.92)
- raffraichissement cache sur enregistrer l'ordre


2012-10-05 - Version 0.5.4

- Correction d'un bug sur les identifiants avec numéros


2012-07-24 - Version 0.5.0

- modification de la version minimale d'ovidentia
- ajout d'un cache



Version 0.4.7

- Correction d'un bug sur la règle automatique du rewriting


Version 0.4.5

 Correction pour l'insertion du lien dans la section utilisateur


2011-05-02 - Version 0.4.3

- Correction rewriting


2011-03-31 - Version 0.4.1

- Correction d'un problème sur le script de déplacement des thèmes à partir de la l'arbre du plan du site
  le problème se présentais si l'arbre contenais un thème sous un thème.
  
 - Version 0.4.0
 
- Modification de la version minimal d'ovidentia
- ajout de la possiblité de mettre un lien sur les conteneur
- ajout de contener ovml pour lister les sous thèmes d'un noeud
  

2011-01-11 - Version 0.3.4

- Permet de désactiver la modification des thèmes lors de l'enregistrement d'un noeud de type theme 


2011-01-08 - Version 0.3.3

- Nouveau script pour déplacer les thèmes et catégories dans la vue administrateur


2010-12-30 - Version 0.3.0

- T3523 : toujours le meme probleme de titre de rubrique visible alors que les urls ne font pas partie des droits de l utilisateur


2010-12-28 - Version 0.2.9

- T3515 : Bug Editeur de plan de site


2010-12-14 - Version 0.2.8

- 0002364: [Undefined] L'ordre n'est pas pris en compte dans l'arbre présenté dans l'éditeur
- 0002363: [Undefined] L'ajout d'un thème dans l'éditeur ajoute bien le thème dans l'arbo, mais sans son nom 
- 0002319: Erreur SQL lors du ré-ordenemencement


2010-11-26 - Version 0.2.5

 - new tree view for user editor based only on modifications in sitemap, this allow modification of non accessibles items


2010-11-24 - Version 0.2.4

 - link with application addon


2010-10-28 - Version 0.2.2

 - add the javascript code to open popup for the nodes with the new window option


2010-10-13 - Version 0.1.5

 - Access rights verification while merging modified sitemap with the core sitemap


Version 0.1.4

 - Now, it is possible to move a node
