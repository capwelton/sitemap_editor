<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
bab_functionality::get('Widgets')->includePhpClass('Widget_BusinessApplicationList');
bab_functionality::includefile('Icons');




class smed_SiteMapList extends Widget_BusinessApplicationList {
	
	private $baseurl;
	
	public function __construct() {
		parent::__construct();
		$this->baseurl1 = smed_getAddon()->getUrl().'admin&idx=edit';
		$this->baseurl2 = smed_getAddon()->getUrl().'admin&idx=tree';
		
		$this->addColumn('edit'			, smed_translate('Edit'), '', false, false);
		$this->addColumn('name'			, smed_translate('Name'));
		$this->addColumn('description'	, smed_translate('Description'));
		$this->addColumn('tree'			, smed_translate('Tree'), '', false, false);
		
		$this->setRessource(smed_sitemapGetRessource(), 50);
		
		
	}
	
	
	


	public function getNextRow() {
		global $babDB;
		if ($arr = $babDB->db_fetch_assoc($this->res)) {
			
			$W = bab_functionality::get('Widgets');
			
		
			$arr['edit'] = $this->editLink(bab_url::mod($this->baseurl1, 'id_sitemap', $arr['id']));
			
			
			$arr['tree'] = $W->Link($W->Icon(smed_translate('Edit tree'), Func_Icons::ACTIONS_VIEW_LIST_TREE), bab_url::mod($this->baseurl2, 'id_sitemap', $arr['id']));
			
			if ('1' === $arr['id'])
			{
				$arr['tree'] = $W->FlowItems(
					$arr['tree'],
					$W->Link($W->Icon(smed_translate('Edit custom tree'), Func_Icons::ACTIONS_VIEW_LIST_TREE), smed_getAddon()->getUrl().'main&idx=node:display&node=Custom')
				);
			}
			

			return $arr;
		}
		return false;
	}
}




