<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/node.class.php';


function smed_onUserDeleted(bab_eventUserDeleted $event) {


}


/**
 * before sitemap creation
 * registered on this event to add more nodes to core sitemap
 *
 * @todo 	move code to core, add methods on event : addAdminLink, addUserLink ...
 *
 * @param	bab_eventBeforeSiteMapCreated 	$event
 */
function smed_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event)
{
    if (!bab_isUserLogged()) {
        return;
    }
    
    require_once dirname(__FILE__).'/node.class.php';
    
    $rootNode = smed_Node::getFromId(1, 'Custom');

    // TODO: test if allowed to edit at least one node or to create nodes
    if (!$rootNode->canCreate() && !bab_isUserAdministrator()) {
        return;
    }

    // add node to access sitemap editor

    $delegations = bab_getUserSitemapDelegations();


    foreach ($delegations as $key => $deleg) {

        $dg_prefix 	= 'bab';
        $dg_prefix2 = 'smed';

        $position = array('root', $key, $dg_prefix.'User', $dg_prefix.'UserSection');

        $item = $event->createItem($dg_prefix2.'User');

        $item->setLabel(smed_translate('Site map editor'));
        $item->setDescription(smed_translate('Change the menus of your site'));
        $item->setLink(smed_getAddon()->getUrl().'main&idx=node.display&node=Custom');
        $item->setPosition($position);
        $item->addIconClassname('apps-sitemap');

        $event->addFunction($item);
    }
}





class smed_siteMapEntry extends bab_siteMap {

    private $id_sitemap = null;

    public function __construct($id, $name, $description)
    {
        $this->id_sitemap = $id;
        parent::__construct($name, $description);
    }

    /**
     *
     * @return bab_siteMapOrphanRootNode
     */
    public function getRootNode($path = null, $levels = null, $build = true)
    {
        require_once dirname(__FILE__).'/sitemap.class.php';
        $rootNode = smed_Sitemap::get($this->id_sitemap, $build);


        return $rootNode;
    }


    public function getLevelRootNode($nodeId)
    {
        require_once dirname(__FILE__).'/sitemap.class.php';
        $rootNode = smed_Sitemap::getLevel($this->id_sitemap, $nodeId);

        return $rootNode;
    }
}




/**
 * collect the list of available sitemaps
 *
 * @param	bab_eventBeforeSiteMapList	$event
 *
 *
 */
function smed_onBeforeSiteMapList(bab_eventBeforeSiteMapList $event)
{
    global $babDB;

    $res = smed_sitemapGetRessource();

    while ($arr = $babDB->db_fetch_assoc($res)) {

        $sitemap = new smed_siteMapEntry($arr['id'], $arr['name'], $arr['description']);
        $basenode = 1 == $arr['id'] ? 'Custom' : null;

        $event->addSiteMap($arr['name'], $sitemap, $basenode);
    }
}



/**
 * Selects a specific theme if the current page is an addon page.
 *
 * Must be executed with a high priority on the BeforePageCreated event to ensure that the skin event
 * is called after this one.
 */
function smed_applySkin(bab_eventBeforePageCreated $event)
{
    $tg = bab_rp('tg', null);

    $addon = bab_getAddonInfosInstance('sitemap_editor');


    if (substr($tg, 0, strlen('addon/' . $addon->getName() . '/')) === 'addon/' . $addon->getName() . '/'
        || substr($tg, 0, strlen('addon/' . $addon->getId() . '/')) === 'addon/' . $addon->getId() . '/') {

        $skinName = 'sitemap_editor';
        $skinCss = 'blue.css';

        bab_skin::applyOnCurrentPage($skinName, $skinCss);
    }
}
