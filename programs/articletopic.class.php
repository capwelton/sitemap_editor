<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/artapi.php';
require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';



/**
 * Article topic creation modification
 */
class smed_ArticleTopic
{
	private $parent_node;

	public function __construct(smed_Node $parent_node)
	{
		$this->parent_node = $parent_node;
	}

	/**
	 * Create category path if necessary and return the leaf category
	 * @return int
	 */
	private function getCategory()
	{
		$ancestors = $this->parent_node->getModifiedAncestors();
		$ancestors[] = $this->parent_node;

		$id_category = 0;
		foreach($ancestors as $ancestor)
		{
			$category_name = $ancestor->getName();

			/*@var $ancestor smed_Node */

			$cat = smed_getCategoryByName($id_category, $category_name);

			if (null === $cat)
			{
				// the category does not exists, create
				$id_category = bab_addTopicsCategory($category_name, $ancestor->getDescription(), 'N', '', '', $id_category);

			} else {
				$id_category = $cat;
			}
		}

		return $id_category;
	}

	/**
	 * Creates a topic under a tree of categories, and sets view access rights to all users.
	 *
	 * @param 	smed_Node	$parent_node
	 * @param	string		$name
	 * @param	string		$description
	 *
	 * @return int	The new topic id.
	 */
	public function create($name, $description, $options)
	{



		$id_category = $this->getCategory();

		$error = '';
		$topicId = bab_addTopic($name, $description, $id_category, $error, $options);
		if ($topicId == 0) {
			throw new Exception('The article topic could not be created.' . "\n" . $error);
		}

		if (isset($options['access_rights']))
		{
			$this->setAccessRights($topicId, $options['access_rights']);
		}

		return $topicId;
	}





	/**
	 *
	 * @param smed_Node $parent_node
	 * @param int $topic
	 * @return unknown_type
	 */
	public function update($topic, $name, $description, $options)
	{
		$id_category = $this->getCategory();

		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/sitemap_editor/');
		$moveAdminTopic = $registry->getValue('moveAdminTopic', true);

		if (!$moveAdminTopic)
		{
			$savedTopic = bab_getTopicArray($topic);
			$id_category = (int) $savedTopic['id_cat'];
		}
		
		
		$topicArr = array(
			'idsaart' => $options['idsaart'],
			'allow_attachments' => $options['allow_attachments'],
			'allow_addImg' => $options['allow_addImg'],
			'allow_meta' => $options['allow_meta']
		);
		
		

		if ($name) {
    		$error = null;
    		if (!bab_updateTopic($topic, $name, $description, $id_category, $error, $topicArr)) {
    		    throw new Exception($error);
    		}
		}
		
		if (isset($options['access_rights']))
		{
			$this->setAccessRights($topic, $options['access_rights']);	
		}
	}
	
	
	protected function setAccessRights($topic, $access_rights)
	{
		aclSetRightsString(BAB_TOPICSVIEW_GROUPS_TBL, $topic, $access_rights['topicsview']);
		aclSetRightsString(BAB_TOPICSSUB_GROUPS_TBL, $topic, $access_rights['topicssub']);
		aclSetRightsString(BAB_TOPICSMOD_GROUPS_TBL, $topic, $access_rights['topicsmod']);
		aclSetRightsString(BAB_TOPICSMAN_GROUPS_TBL, $topic, $access_rights['topicsman']);
		aclSetRightsString(BAB_TOPICSCOM_GROUPS_TBL, $topic, $access_rights['topicscom']);
	}

}