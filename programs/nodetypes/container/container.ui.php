<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




class smed_ContainerNodeEditor extends smed_NodeEditor
{

    public function __construct($id = null, Widget_Layout $layout = null, smed_Node $node = null)
    {
        parent::__construct($id, $layout, $node);

        $W = $this->widgets;

        $this->generalSection->addItem($this->Description(),0);
        $this->generalSection->addItem($this->Label(), 0);

        $this->generalSection->addItem($this->Url());

        $this->setHiddenValue('node[type]', 'container');
        
        $this->advancedSection->addItem($this->setlanguage());
    }

    public function Url()
    {
        $W = $this->widgets;

        $select = $this->labelledField(
            smed_translate('Associated page'),
            $widget = $W->Select(),
            'url'
        );

        $widget->addOption('smed_custom_url', smed_translate('Custom url'));

        // add predefined pages

        $widget->addOption('?tg=oml&file=addons/sitemap_editor/containerItemsList.html', smed_translate('List sub-levels'));
        $widget->addOption('?tg=oml&file=addons/sitemap_editor/containerItemsAndRecentsArticles.html', smed_translate('Sub-levels and last articles'));
        $widget->addOption('?tg=oml&file=addons/sitemap_editor/firstItem.html', smed_translate('Same as first child node'));

        // add skin proposed pages


        $lineedit = $W->LineEdit('smed_custom_url')->setName('custom_url')->setSize(50);
        $widget->setAssociatedDisplayable($lineedit);

        return $W->FlowItems($select, $lineedit)->setHorizontalSpacing(2,'em')->setVerticalAlign('bottom');
    }
    
    
    
    public function setlanguage()
    {
        $W = $this->widgets;
        
        $options = array('' => '');
        foreach(bab_getAvailableLanguages() as $lang) {
            $options[$lang] = $lang;
        }
        
        return $this->labelledField(
            smed_translate('Set site language if in url'), 
            $W->Select()->setOptions($options),
            __FUNCTION__
        );
    }

}
