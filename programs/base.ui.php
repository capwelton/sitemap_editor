<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';



smed_Widgets()->includePhpClass('widget_Form');
smed_Widgets()->includePhpClass('widget_Frame');
smed_Widgets()->includePhpClass('widget_InputWidget');
smed_Widgets()->includePhpClass('widget_TableView');





/**
 * @return Widget_Form
 */
class smed_Editor extends Widget_Form
{
    protected $ctrl;

    protected $saveAction;
    protected $saveLabel;

    protected $cancelAction;
    protected $cancelLabel;

    /* var Func_Widgets */
    protected $widgets;

    protected $buttonsLayout = null;
    protected $innerLayout = null;




    /**
     * @param 	Widget_Layout 	$layout		The layout that will manage how widgets are displayed in this form.
     * @param 	string 			$id			The item unique id.
     */
    public function __construct($id = null, Widget_Layout $layout = null)
    {
         // We simulate inheritance from Widget_VBoxLayout.
        $W = $this->widgets = smed_Widgets();

        if (isset($layout)) {
            $this->innerLayout = $layout;
        } else {
            $this->innerLayout = $W->VBoxLayout();//->setVerticalSpacing(2, 'em');
        }

        $layout = $W->VBoxItems(
            $this->innerLayout->addClass('smed-loading-box') // javascript can add the crm-loading-on class to this container to show ajax loading progress in the form
        );
        $layout->setVerticalSpacing(1, 'em');
        parent::__construct($id, $layout);




        $this->prependFields();

//		$this->addClass('smed_edit');
    }


    /**
     *
     * @return smed_Editor
     */
    public function setController($ctrl)
    {
        $this->ctrl = $ctrl;
        return $this;
    }


    /**
     *
     * @return smed_Editor
     */
    public function setSaveAction($saveAction, $saveLabel = null)
    {
        $this->saveAction = $saveAction;
        $this->saveLabel = $saveLabel;
        return $this;
    }

    /**
     *
     * @return smed_Editor
     */
    public function setCancelAction($cancelAction, $cancelLabel = null)
    {
        $this->cancelAction = $cancelAction;
        $this->cancelLabel = $cancelLabel;
        return $this;
    }




    /**
     * Adds a button to button box of the form.
     *
     * @return smed_Editor
     */
    public function addButton(Widget_SubmitButton $button, Widget_Action $action = null)
    {
        if (!isset($this->buttonsLayout)) {
            $this->buttonsLayout = $this->widgets->FlowLayout()
                ->addClass('smed-form-buttons')
                ->setHorizontalSpacing(1, 'em');
            parent::addItem($this->buttonsLayout);
        }
        $this->buttonsLayout->addItem($button);
        if (isset($action)) {
            $button->setAction($action);
        }

        return $this;
    }


    /**
     * Adds an item to the top part of the form.
     *
     * @return smed_Editor
     */
    public function addItem(Widget_Displayable_Interface $item = null, $position = null)
    {
        $this->innerLayout->addItem($item, $position);

        return $this;
    }


    /**
     *
     *
     */
    protected function appendButtons()
    {
        $W = $this->widgets;

        if (isset($this->saveAction)) {
            $saveLabel = isset($this->saveLabel) ? $this->saveLabel : smed_translate('Save');
            $this->addButton(
                $W->SubmitButton(/*'save'*/)
                    ->validate(true)
                    ->setAction($this->saveAction)
                    ->setLabel($saveLabel)
            );
        }

        if (isset($this->cancelAction)) {
            $cancelLabel = isset($this->cancelLabel) ? $this->cancelLabel : smed_translate('Cancel');
            $this->addButton(
                $W->SubmitButton(/*'cancel'*/)
                    ->setAction($this->cancelAction)
                      ->setLabel($cancelLabel)
            );
        }

    }

    /**
     * Fields that will appear at the beginning of the form.
     *
     */
    protected function prependFields()
    {

    }


    /**
     * Fields that will appear at the end of the form.
     *
     */
    protected function appendFields()
    {

    }


    /**
     * Adds an item with a label and a description to the form.
     *
     * @param	string			$label
     * @param	Widget_Item		$item
     * @param	string			$fieldName
     * @param	string			$description
     *
     * @return smed_Editor
     */
    public function labelledField($labelText, Widget_Item $item, $fieldName = null, $description = null)
    {
        $W = $this->widgets;

        $labelledItem = new smed_LabelledWidget($labelText, $item);

        if (isset($fieldName)) {
            $labelledItem->setName($fieldName);
        }

        if (isset($description)) {
            $labelledItem->setDescription($description);
        }

        return $labelledItem;
    }


    /**
     *
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->appendFields();
        if (!$this->isDisplayMode()) {
            $this->appendButtons();
        }
        return parent::display($canvas);
    }

}











class smed_LabelledWidget extends Widget_InputWidget implements Widget_Displayable_Interface
{
    private $labelText = '';
    private $label = null;
    private $inputWidget = null;

    /**
     *
     * @param string		$label
     * @param Widget_Item	$item
     * @param string		$id
     * @param Widget_Layout $layout
     */
    public function __construct($labelText, Widget_Widget $inputWidget, $id = null)
    {
        $this->labelText = $labelText;
        $this->inputWidget = $inputWidget;
        $W = smed_Widgets();



        $this->layout = $this->createFieldLayout();
        parent::__construct($id);
    }


    private function createFieldLayout()
    {
        $W = smed_Widgets();

		$this->label = $W->Label($this->labelText)
			->addClass('smed-description');

        if ($this->inputWidget instanceof Widget_InputWidget) {
            $this->label->setAssociatedWidget($this->inputWidget);
        }


        $layout = $W->VBoxLayout()
                ->addClass('smed-form-field')
                ->setVerticalSpacing(0.2, 'em');


        if ($this->inputWidget instanceof Widget_CheckBox) {
            $h = $W->HBoxLayout()
                ->setVerticalAlign('middle')
                ->setHorizontalSpacing(0.2, 'em')
                ->addItem($this->inputWidget)
                ->addItem($this->label->colon(false));

            $layout->addItem($h);

        } else {

            $layout
                ->addItem($this->label)
                ->addItem($this->inputWidget);
        }

        return $layout;
    }


    public function setParent($parent)
    {
        $this->layout->setParent($parent);
    }

    public function getParent()
    {
        return $this->layout->getParent();
    }

    /**
     *
     * @param mixed $value
     * @return smed_LabelledWidget
     */
    public function setValue($value)
    {
        if (isset($this->inputWidget)) {
            $this->inputWidget->setValue($value);
        }
        return $this;
    }

    /**
     *
     * @return mixed;
     */
    public function getValue()
    {
        if (isset($this->inputWidget)) {
            return $this->inputWidget->getValue();
        }
        return null;
    }

    /**
     *
     * @param string $name
     * @return smed_LabelledWidget
     */
    public function setName($name)
    {
        if (isset($this->inputWidget)) {
            $this->inputWidget->setName($name);
        }
        return $this;
    }

    /**
     *
     * @return string;
     */
    public function getId()
    {
        if (isset($this->inputWidget)) {
            return $this->inputWidget->getId();
        }
        return parent::getId();
    }

    public function testMandatory()
    {
        if (isset($this->inputWidget)) {
            return $this->inputWidget->testMandatory();
        }
        return parent::testMandatory();
    }


    /**
     *
     * @return string;
     */
    public function getName()
    {
        if (isset($this->inputWidget)) {
            return $this->inputWidget->getName();
        }
        return null;
    }


    /**
     *
     *
     * @return Widget_InputWidget
     */
    public function getInputWidget()
    {
        return $this->inputWidget;
    }


    /**
     *
     * @return smed_LabelledWidget
     */
    public function colon($active)
    {
        if (isset($this->label)) {
            $this->label->colon($active);
        }
        return $this;
    }
    /**
     *
     * @param Widget_Canvas $canvas
     * @return string
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = smed_Widgets();


        return $this->layout->display($canvas);
    }


    public function setDescription($description)
    {
        $W = smed_Widgets();
        $this->layout->addItem($W->Label($description)->addClass('crm-long-description'));
    }






    /**
     * Replace item by another
     *
     * @param string $oldId
     * @param Widget_Displayable_Interface $item
     * @return bool
     */
    public function replaceItem($oldId, Widget_Displayable_Interface $item)
    {

        if ($this->inputWidget->getId() === $oldId) {
            $this->inputWidget = $item;

            $this->layout = $this->createFieldLayout();
            return true;
        }

        return false;
    }
}





class smed_Toolbar extends Widget_Frame
{
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        if (!isset($layout)) {
            $W = smed_Widgets();
            $layout = $W->FlowLayout()->setHorizontalSpacing(1, 'em');
            $layout->setVerticalAlign('top');
        }

        parent::__construct($id, $layout);
        $this->addClass('smed-toolbar')
                ->addClass('icon-left-16 icon-16x16 icon-left');
    }

    /**
     *
     * @param string		 	$labelText
     * @param string			$iconName
     * @param Widget_Action		$action
     * @param string			$id
     * @return smed_Toolbar
     */
    public function addButton($labelText = null, $iconName = null, $action = null, $id = null)
    {
        $W = smed_Widgets();
        if (isset($iconName)) {
            $content = $W->Icon($labelText, $iconName);
        } else {
            $content = $labelText;
        }
        $button = $W->Link($content, $action, $id);

        $this->addItem($button);

        return $this;
    }


    public function addInstantForm($form, $labelText = null, $iconName = null, $action = null, $id = null)
    {
        $W = smed_Widgets();
        if (isset($iconName)) {
            $content = $W->Icon($labelText, $iconName);
        } else {
            $content = $labelText;
        }
        $button = $W->Link($content, $action, $id);

        $this->addItem(
            $W->VBoxItems(
                $button->addClass('crm-instant-button'),
                $form->addClass('crm-instant-form')
            )->addClass('crm-instant-container')
        );

        return $this;

    }
}





smed_Widgets()->includePhpClass('Widget_BabPage');


/**
 *
 */
class smed_Page extends Widget_BabPage
{
    /**
     * @var Widget_Layout
     */
    private $mainPanel = null;

    /**
     * @var Widget_Frame
     */
    private $context = null;


    public function __construct($id = null, Widget_Layout $layout = null)
    {
        parent::__construct($id, $layout);

        $W = bab_functionality::get('Widgets');

        /* @var $I Func_Icons */
        $I = bab_Functionality::get('Icons');
        if ($I) {
            $I->includeCss();
        }

        $this->addClass('crm-page');

        $addon = bab_getAddonInfosInstance('sitemap_editor');
        $this->addStyleSheet($addon->getStylePath().'main.css');
        $this->addJavascriptFile(smed_getAddon()->getTemplatePath().'scripts.js');



        $this->mainPanel = $W->VBoxLayout();


        // add error message if the sitemap is not selected


        require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';
        $settings = bab_getInstance('bab_Settings');
        /*@var $settings bab_Settings */

        $site = $settings->getSiteSettings();
        $sitemap = new smed_Sitemap(1);
        $smed = $sitemap->getValues();

        if ($site['sitemap'] !== $smed['name'])
        {
            $this->addError(sprintf(smed_translate('The sitemap "%s" is not selected in site options, modifications will not be effective'), $smed['name']));
        }
    }

    /**
     * Add item to page
     * @param	Widget_Item $item
     * @return	smed_Page
     */
    public function addItem(Widget_Displayable_Interface $item = null)
    {
        $this->mainPanel->addItem($item);
        return parent::addItem($item);
    }


    /**
     * Add the item to the context panel of the page
     * @param	Widget_Item $item
     * @return	smed_Page
     */
    public function addContextItem(Widget_Displayable_Interface $item, $title = null)
    {
        if (null === $this->context) {

            $W = bab_functionality::get('Widgets');

            $this->context = $W->Frame(
                'smed_context_panel',
                $W->VBoxLayout()->setVerticalSpacing(2, 'em')
            )->setCanvasOptions(Widget_Item::Options()->width(28, 'em'));
        }
        if (isset($title)) {
            $this->context->addItem(
                $W->VBoxItems(
                    $W->Title($title, 4),
                    $item
                )
            );
        } else {
            $this->context->addItem($item);
        }
        return $this;
    }


    public function display(Widget_Canvas $canvas)
    {
        if ($this->context) {

            $W = bab_functionality::get('Widgets');

            $this->setLayout(
                $W->HBoxItems(
                    $W->Frame('smed_main_panel', $this->mainPanel),
                    $this->context
                )
            );

            $this->addClass('crm-two-panel-page');
        } else {

            $this->setLayout($this->mainPanel);
        }

        return parent::display($canvas);
    }


    /**
     * @return widget_VBoxLayout
     */
    public function ActionsFrame()
    {
        $W = bab_functionality::get('Widgets');
        return $W->VBoxLayout()
            ->setVerticalSpacing(3, 'px')
            ->addClass('icon-left-16 icon-16x16 icon-left')
            ->addClass('crm-actions');
    }
}

